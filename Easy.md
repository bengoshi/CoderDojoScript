Contents

Bildnachweis {#bildnachweis .unnumbered}
============

Deckblatt: Noma Dojo in Tokyo - Von Wang Ming

:   \
    <http://en.wikipedia.org/wiki/Image:Noma_Dojo%2C_2006.JPG>, CC BY-SA
    3.0, https://commons.wikimedia.org/w/index.php?curid=2581123

Einführung

:   \
    Kinder in dem philippinischen Karate-Dojo der Jack und Jill Schule
    in Bacolod - Von Jjskarate. - Eigenes Werk; Übertragen aus
    en.wikipedia nach Commons.., CC BY-SA 3.0,
    https://commons.wikimedia.org/w/index.php?curid=50279796

Einführung

:   \
    Karatetraining - Image by
    <https://pixabay.com/users/stivy73-6360334/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2717178>Michele
    Stival from
    <https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2717178>Pixabay

Einführung

:   \
    Kindertraining - Image by
    <https://pixabay.com/users/publicdomainpictures-14/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=83009>
    PublicDomainPictures from
    <https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=83009>
    Pixabay

Einführung

:   \
    Kentern - Image by
    <https://pixabay.com/users/clker-free-vector-images-3736/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=31696>
    Clker-Free-Vector-Images from
    <https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=31696>
    Pixabay

Frontseite: Screenshots - Eigenproduktion

:   \

7\. Kyo - Schleifen

:   \
    Von OCAD U - OCAD U, CC BY-SA 3.0,
    <https://commons.wikimedia.org/w/index.php?curid=17141667>

7\. Kyo - Strings

:   \
    Solarmodul - Gemeinfrei,
    <https://commons.wikimedia.org/w/index.php?curid=64041>

7\. Kyo - Browser

:   \
    Kinder am Rechner Image by
    <https://pixabay.com/users/startupstockphotos-690514/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=593313>
    StartupStockPhotos from
    <https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=593313>
    Pixabay

7\. Kyo - ¸Hilfen

:   \
    Image by
    <https://pixabay.com/users/tumisu-148124/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2444110>
    Tumisu from
    <https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2444110>
    Pixabay

7\. Kyo - Kultur

:   \
    Von wachowski brothers -
    http://www.seeklogo.com/the-matrix-logo-138764.html, Logo,
    https://de.wikipedia.org/w/index.php?curid=6070665

    sonstige Angaben {#sonstige-angaben .unnumbered}
    ----------------

    Klar verwenden wir eine geschlechtergerechte Sprache. Wir verwenden
    den Doppelpunkt statt des Sterns, da er von Screenreadern besser
    unterstützt wird. Mal kann es auch das generische Maskulinum, mal
    das generische Femenin geben. Wenn Du denkst, dass wir diesen Ansatz
    an der einen oder anderen Stelle besser umgesetzt wäre, schreib uns
    an.

Einführung {#einführung .unnumbered}
==========

Dummerweise kann man niemanden erklären,\
was die Matrix ist.\
Du musst sie selbst erleben.

The Matrix\

Willkommen zum Kursbuch des CoderDojo. Während die meisten CoderDojos
auf Kinder ausgerichte sind, liegt unser Fokus bei Jugendlichen. Wir
nehmen das nicht so genau, aber wenn Du jünger als 12 bis 14 Jahren
bist, wirst wahrscheinlich mit dem CoderDojo Berlin besser beraten sein.
Dort findest Du Gleichaltrige und wirst Programmieren mit einer
graphischen Oberfläche lernen. Die schauen wir uns zwar auch einmal an,
aber es stellt nicht unseren Schwerpunkt dar. Für diejenigen, die 16
Jahre und älter sind gibt es immer wieder zusätzliche Aufgaben, die
mathematisch orientiert sind. Daran kannst Du zusätzlich knobeln und
weiter trainieren.

Vorab - wer jetzt denkt - wofür ein weiterer Python-Buch bzw. -Kurs?
Gibt es davon nicht schon genug? Ja, das ist richtig. Aber wir streben
weder einen Selbstlernkurs an, noch setzen wir auf Unterricht wie in der
Schule. Unser Ansatz ist eine Mischung aus zu Hause hacken und
gemeinsamen Austausch. Es gibt aber Dinge, bei denen die Erfahrung
gezeigt hat, dass da viele das Handtuch schmeißen, beispielsweise die
Installation. Lass Dir bei so was bitte helfen, teilweise setzen wir die
Inanspruchnahme der Hilfe sogar ausdrücklich voraus. Und bei uns geht es
mehr als nur „schnöden" Code. Wir schauen auf Dinge wie Datenschutz,
Nerdkultur ebenso wie auf Hardware.

r9cm ![image](Pictures/JJS_Dojo.jpg){width="8.5cm"}

Trainieren? Der Begriff Dojo kommt aus dem Kampfsport und stellt dort
den Trainingsraum oder -halle dar. Programmieren lernen hat vieles damit
gemeinsam. Wenn Du Programmieren lernen willst, wird das nicht klappen,
indem Du nur ein Buch liest oder einen Film schaust. So lernst Du weder
Karate, Judo oder Fahrrad fahren. Da gehört immer eine ordentliche
Portion Übung dazu. Karate oder Judo lernen sich nicht alleine. Man kann
eine Kata, also die vorgeschrieben Bewegungsabläufe, alleine üben. Aber
regelmäßige Partnerübungen sind genauso wichtig wie das sich jemand von
außen das anschaut und Dir hilft, Deine Bewegungen zu verbessern. So ist
das beim Coden auch. Es hilft, sich gegenseitig Code vorzustellen, sich
zu besprechen und sich Unterstützung zu holen, wenn es mal klemmt. Und
jeder macht früher oder später die Erfahrung, dass es mal nicht
weitergeht. Statt das Handtuch zu werfen, lernt Ihr Euch gegenseitig zu
helfen oder von den erfahrenen Mentoren unterstützt zu werden. Dieser
Teil ist mindestens so bedeutsam wie alleine zu frickeln. Und am meisten
lernt, wer lehrt - Du wirst von Problemen anderer hören, kannst ihnen
helfen und eine Menge selbst dazu lernen. Aktuell treffen wir uns einmal
wöchentlich in einer Videokonferenz. Das wollen wir ergänzen durch ein
einmal monatliches Treffen im Berliner Hackerspace xHain. An dieser
Stelle kann das Script beziehungsweise der Text nicht immer aktuell sein
- also schau bitte auf unserer Homepage unter News nach. Wenn Du nicht
aus Berlin kommst, sprich uns an und wir versuchen Dir zu helfen, in
Deiner Stadt Ansprechpartner zu finden.

Kennst Du die Gürtelfarben aus dem Kampfsport? Ein bunter Strauß an
Farben, der Dich am Ende zum schwarzen Gürtel führen soll, die
sogenannten Kyo-Grade oder im deutschen Schüler-Grade. Es gibt nicht nur
einen schwarzen Gürtel, sondern verschiedene Dan-Grade - im deutschen
Meister-Grade. An diesem System wollen wir uns orientieren. Wir beginnen
mit dem Weg zum weißen Gürtel. Danach hast Du die wichtigsten bzw.
ersten Grundbegriffe drauf. Wir haben keine „Gürtelprüfungen" geplant.
Aus eigenen Erfahrungen von verschiedenen Budo-Sportarten finden das
manche jedoch gut. Wenn Euch so etwas ansprechen sollte, sagt uns
Bescheid und wir gehen darauf ein. Ob mit oder ohne Prüfung: Du solltest
die Tests für den „Gürtel" bestehen, bevor Du weitermachst. Andernfalls
fehlen Dir Grundlagen für die nächsten Kapitel und das schafft mehr
Frust als Lust beim Weitermachen. Wenn Du merkst, dass Dir was fehlt und
da nicht so richtig rankommst - sprich uns an, damit wir gemeinsam daran
feilen. Umgekehrt hilft es Mentoren, wenn sie wissen, was Du schon
gemacht hast und was vorausgesetzt werden darf. Wenn Du bei einem
solchen „Test" nicht weiterkommst, ist das nicht schlimm - wir sind hier
nicht in der Schule. Hier geht es nicht darum, eine Prüfung zu bestehen,
sondern etwas zu lernen. Wenn Du also bei einem Test stecken bleibst,
nimm das zum Anlass, um Dir von den Mentorinnen helfen zu lassen. Oft
braucht es nur einen kleinen Stupser in die richtige Richtung und Du
kannst die Aufgabe selber richtig weiterlösen.

l7cm ![image](Pictures/karate-2717178_1280.jpg){width="6.5cm"}

Was lernen wir? Wir beginnen mit Python. Die Wahl der „richtigen"
Programmiersprache kann zu regelrechten Glaubensstreitigkeiten führen.
Es gibt Sprachen, die sich eher für Anfänger eignen und welche, die sich
weniger für Anfänger eigenen. Und es Sprachen, die für einen bestimmten
Zweck besser geeignet sind als andere. Es gibt nicht *"die"*
Programmiersprache. Insofern werden wir später auch einen Blick auf
andere Sprachen werfen. Es sind keine Programmiersprachen im
eigentlichen Sinne, aber html und css, um Internetseiten zu erstellen
werden wir erlernen. Wir werden ebenso einen Blick auf Dinge wie
Netzwerktechnik, auf Linux, Datenschutz, Cybersicherheit werfen und
nehmen bei unseren Treffen in unserem Hackspace oder auf dem Congress
gerne einmal einen Lötkolben in die Hand. Der Weg zum *"schwarzen
Gürtel"* besteht also nicht nur aus dem Erlernen von ein paar Befehlen
einer Programmiersprache, sondern aus einem Potpourri an Dingen. Und Dir
sollte klar sein - zum begehrten *"schwarzen Gürtel"* ist noch niemand
über 's Wochenende gekommen. Gib Dir also selbst Zeit.

Der Anfang ist leider oft etwas trocken, weil man erst etwas Rüstzeug
lernen muss, bevor es richtig spaßig wird. Bevor Du tolle Würfe und
Tritte lernst, steht immer erst eine Runde Fallschule und Bewegungslehre
an. So ist das hier auch. Im Weißgurt lernst Du absolute Basics. Vieles
werden wir nur anreißen können und müssen Dich für eine Vertiefung auf
später vertrösten. Aber andernfalls besteht die Gefahr, sich gleich in
Details zu verlieren. Auch in den folgenden Einheiten werden wir aus
diesem Grund manche Dinge bewusst auf spätere Einheiten verschieben. Im
Geldgurt wollen wir dann aber gleich ein Weltraumspiel bauen und dieses
Stück für Stück weiter ausbauen. Gleichzeitig wollen wir keine Dinge
machen, die anfangs noch nicht erklärt werden können. Aus diesem Grund
verzichten wir zunächst auf grafische „Spielereien". Aber halte durch,
die kommen!

r10cm ![image](Pictures/martial-83009_1280.jpg){width="9.5cm"}

Wie oft solltest Du Dich damit beschäftigen müssen? Müssen wäre schon
mal kein guter Start. Wir sind hier nicht in der Schule. Wir treffen
uns, weil wir neugierig sind und Spaß an der Sache haben, nicht weil wir
müssen. Insofern kann man selbstverständlich auch mal aussetzen, weil es
vielleicht einem gerade mit Klausuren zuviel wird oder anderes anliegt.
Aber Dir sollte klar sein, dass ohne eine regelmäßige Beschäftigung Du
nicht voran kommen wirst und es wenig motiviert, wenn man immer auf der
Stelle tritt. Neben unserem wöchentlichen Treffen solltest Du Dir also
schon mindestens einen Nachmittag die Woche dafür Zeit nehmen. Zwar
setzt sich niemand hin und paukt wie bei einer Sprache Vokabeln. Die
Befehle lernst Du, indem Du sie regelmäßig benutzt. Ohne das wird es
schwierig. Wir wollen Dich nicht abschrecken, sondern motiviere, dran zu
bleiben. Der Anfang stellt erfahrungsgemäß eine erste Klippe dar und
danach kommen ebenfalls Höhen und Tiefen. Niemand lernt Segeln, wenn
immer Flaute ist. Böen können anstrengend sein, aber mit jeder lernt man
etwas dazu. Und wenn das Bötchen kentert - nicht schlimm, wieder
aufrichten, Wasser rausschöpfen, weiterfahren.

l6cm ![image](Pictures/capsized-31696_1280.png){width="5.5cm"}

Wir setzen keine Kenntnisse voraus. Wenn Du schon Vorkenntnisse hast und
später einsteigen willst, ist das kein Problem. Mach die Tests am Ende
eines Kapitels (nicht nur lesen, lösen!). Wenn Du die hinbekommst,
weiterziehen. Es gibt noch keine Kapitel? Dann wirst Du Dich leider
gedulden müssen, da der Kurs noch im Entstehen ist. Wenn Du aber schon
soweit bist - beteilige Dich doch und hilf uns, den Kurs
weiteraufzubauen.

Was Du jedoch brauchen wirst, ist eine installierte Python-Version mit
virtualenv und eine Oberfläche, um einen Code zu schreiben. Wir
empfehlen hier die Community-Version von PyCharme. Klar ginge es auch
mit anderen anderen IDEs, aber Du machst es Dir selber leichter, wenn Du
zumindest am Anfang die gleiche nimmst. Wir werden später auch einen
Blick auf andere werfen. Daneben solltest Du Git installiert habenn. Wir
verzichten hier bewusst auf Installationsanleitungen, sondern geben nur
ein paar Links. Es gibt erhebliche Unterschiede zwischen Windows, Linux
und Mac und den dortigen Versionen. Außerdem ist das ein Punkt, an dem
viele verzweifeln und Aussteigen. Um nicht an dieser Hürde zu scheitern
- wenn Du es nicht selber hinbekommst, lass Dir bitte helfen. Vielleicht
hast Du jemanden in Deinem Umfeld wie Deine Eltern oder Lehrkräfte.
Ansonsten bieten wir Dir gerne an, bei der Installation zu helfen.
Sprich uns einfach an. Du verfügst nur über ein Handy oder Tablett und
kannst auf keinen Computer / Notebook zugreifen? Wir helfen Dir gerne,
wie Du für circa 100 Euro für die ersten Schritte ausreichendes Gerät
erwerben kannst. Wenn Geburtstag oder Weihnachten gerade noch weit weg
sind oder Du erstmal schauen möchtest, ob das überhaupt Dein Ding ist -
wir haben einen kleinen Pool an Leihgeräten. Scheu Dich nicht, uns
ansprechen. Wir helfen Dir gerne.

Ein paar Hinweise zum Layout. Wenn es um Code geht, sieht das so aus:

print(\"Testcode\")

Wenn Code angegeben wird - probiere ihn bitte immer selbst auch bei Dir
aus! Das übt. Und verändere ihn gerne und spiele damit herum. Du kannst
nichts kaputt machen. Wenn wir Dir sagen - das oder jenes geht nicht -
probiere es mal aus. Auch wenn es anfangs komisch klingen mag - aus
genau den dann entstehenden Fehlermeldungen lernst Du eine Menge!

Wenn Stolperstein zu beachten sind, gibt es an der Seitenrand einen
Hinweis. Hier solltest Du besonders aufpassen und die Hinweise genau
befolgen beziehungsweise nochmal lesen, wenn etwas nicht funktioniert.

Wenn es mathematische Zusatzaufgaben gibt, wird das extra ausgewiesen.

7. Kyo - Weißgurt
=================

Hello World
-----------

Wenn Du PyCharm startest, sollte der Start bei Dir ungefähr so aussehen:

![image](Pictures/8-1-Pycharm-2.png)

Klicke auf „New Project". Dann schaut es ungefähr so aus:

![image](Pictures/8-1-Pycharm-3.png)

Bei der obersten Zeile mit Location änderst Du den Projektnamen auf
„HelloWorld". Das ist Dein Projektname. Die weiteren
Einstellmöglichkeiten hier werden wir mit der Zeit kennen lernen. Dann
klickst Du auf „Create". Unter Umständen rödelt Deine Kiste jetzt einen
Moment. Im nächsten Schritt baut sich der Editor auf. In dem Teil
main.py steht allerhand Zeug. Das löschst Du jetzt einfach - entweder Du
markierst es mit der Maus und drückst die Entf-Taste oder -
Tastaturkürzel lernen ist immer gut - mit Strg+A oder Control+A alles
markieren und dann Entf drücken. Dann sollte das bei Dir ungefähr so
ausschauen:

![image](Pictures/8-1-Pycharm-4.png)

Okay, jetzt kann es losgehen.

Warst Du schon mal in einem Dojo? Falls nicht - immer wenn man ein Dojo
betritt und verläßt gibt es eine kleine Verbeugung als Gruß. Beginnt man
mit einer neuen Programmiersprache, gibt es auch ein solches „Ritual".
Man beginnt damit, den Computer „Hello World" ausgeben zu lassen. Um
etwas ausgeben zu lassen, verwenden wir den Befehl print (engl.
drucken). Das wollen wir jetzt probieren:

print(\"Hello World\")

Jetzt führst Du das Programm aus: Du gehst in der Menüzeile auf „Run"
und dort auf den obersten Punkt „Run 'main'". Dann sollte sich unten ein
Fenster aufbauen, in welchem das Ergebnis Deines Programms ausgeführt
wird.

![image](Pictures/8-1-Pycharm-5.png)

Wenn da „Hello World" steht hast Du es geschafft und Dein erstes
Programm geschrieben. Okay, noch ein sehr kleines, aber immerhin. Wenn
bis hierin nicht gekommen bist - bitte zögere nicht und lass Dir helfen!
Hier können unvorhersehbare Stolpersteine im Weg liegen, die Du
vielleicht nicht ohne etwas Unterstützung aus dem Weg geräumt bekommst.

Frag jemanden aus dem Umfeld, komm beim uns zum Videotreff oder wenn es
Dir möglich ist persönlich mit Deinem Rechner vorbei. Anders gesagt -
wenn das hier nicht läuft, kann der Rest auch nicht laufen. Es ist gut,
wenn Du Dich selber daran versuchst, aber lass Dich nicht entmutigen,
wenn es nicht klappt.

Schleifen
---------

Als nächstes wollen wir dieses „Hello World" zehnmal ausgeben. Eine
einfache Methode wäre jetzt sowas:

print(\"Hello World\") print(\"Hello World\") print(\"Hello World\")
print(\"Hello World\") print(\"Hello World\") print(\"Hello World\")
print(\"Hello World\") print(\"Hello World\") print(\"Hello World\")
print(\"Hello World\")

Aber der gute Admin ist faul - wenn er stupide Dinge mehrfach machen
muss, automatisiert er sie. Und genau hier sind Computer ja auch echt
stark. Zehnmal geht noch gut, wenn Du 100 mal das machen müsstest, wäre
es schon nervig. Und Du müsstest, um sicher zu gehen, dass es nicht 99
oder 101 sind, mehrfach nachzählen. Schöner wäre es doch, wenn wir dem
Computer sagen könnten, dass er diese Befehlszeile n-mal wiederholen
soll. Das machen wir mit einer sogenannten For-Schleife oder For-Loop.

for i in range(10): print(\"Hello World\")

Die Einrückung erzeugst Du, indem Du die Tab-Taste verwendest. Das ist
die Taste zwischen CapsLock (dauernd groß schreiben und der \^-Taste ).
Probiere es gleich mal aus, indem Du wieder auf „Run" und dann auf „Run
'main'" gehst. Zähle die Ausgabe nach. Sind es wirklich zehn „Hello
World"?

Okay, jetzt bist Du dran. Als nächstes bau den Code bitte so um, dass es
zu folgender Ausgabe kommt:

Hello World World World World World

Geschafft? Dann gleich zur nächsten Übung. Versuche folgende Ausgabe zu
erzeugen:

Hello World Hello World Hello World Hello World Hello World

Was passiert in der Schleife?
-----------------------------

Dabei solltest Du „Hello" und „World" jeweils nur einmal schreiben. Wenn
Du da Schwierigkeiten hast, dann einen Hinweis: Zur For-Schleife zählt
alles das, was eingerückt ist. Diese Einrückung ist eine Besonderheit
der Programmiersprache Python. Es gibt Sprachen die solche
zusammenhängenden Blöcke mit begin und end markieren, die meisten
Sprachen verwenden dafür Klammern. Python rückt ein. Hintergrund ist,
dass man dies bei den anderen Programmiersprachen auch machen soll,
damit der Code übersichtlicher und verständlicher wird. Die Tugend ist
bei Python Pflicht. Rücke zwei print-Zeilen unterhalb der For-Schleife
ein und lass sie Dir ausgeben. Dann probierst Du aus, wenn die erste
nicht eingerückt ist und wenn die erste, aber nicht die zweite
eingerückt ist. Der erste Fall wirft Dir einen Fehler aus. Warum? Weil
Python erwartet, dass es zu der Schleife einen Inhalt gibt. Rückst Du
nichts ein, fehlt dieser Inhalt.

r7cm ![image](Pictures/Tie_shoelace.png){width="6.5cm"}

Was passiert jetzt bei dieser Schleife? i ist eine sogenannte Variable.
Das kennst Du bestimmt schon aus dem Matheunterricht. Ihr wird mit jedem
Durchlauf eine Zahl zugewiesen. range(10) gibt an, dass mit jedem
Durchlauf i um eins erhöht werden soll, bis es 10 ist. Klingt wenig
verständlich? Lass Dir einfach mal ausgeben, was in i in jedem Durchlauf
drin steckt.

for i in range(10): print(\"i lautet: \", i)

range kann noch mehr. Folgender Code beziehungsweise dessen Ausgabe
sollte Dir das ganze zeigen:

for i in range(5, 25, 3): print(\"i lautet: \", i)

Du kannst also Startwert, Endwert und Schrittweite definieren. Diese
Zahlen können auch negativ sein. Spiel damit ein wenig rum und gib ein
paar andere Zahlen ein.

Jetzt wollen wir noch eine Übung machen - lass uns einen Tannenbaum als
sogenannte ASCII-Art zeichnen. Wir gehen später noch genauer darauf ein,
was ASCII ist, aber im kurzen sind es die Buchstaben und Zeichen, die Du
so am Computer siehst. Damit kann man kleine Kunstwerke schaffen.
Versuche bitte, dass folgendes ausgegeben wird:

\* \*\*\* \*\*\*\*\* \*\*\*\*\*\*\* \*\*\*\*\*\*\*\*\*
\*\*\*\*\*\*\*\*\*\*\* \*\*\*\*\*\*\*\*\*\*\*\*\* \*\* \*\* \*\*

Die Herausforderung ist es jetzt, mit möglichst wenig Code auszukommen.
Wir zeigen hier bewusst keine Musterlösung. Lasst uns gemeinsam im
CoderDojo darauf schauen, welche unterschiedlichen Wege Ihr so geht.
Dabei lernt Ihr voneinander eine Menge. Es ist immer gut, auch fremden
Code zu sehen.

Und weil ASCII-Art so schön ist, „male" noch eine Sanduhr (knobel auch
hier, wie Du mit möglichst wenig Code auskommst):

\*\*\*\*\* \*\*\* \* \*\*\* \*\*\*\*\*

Benennung von Variablen
-----------------------

Muss das eigentlich ein „i" sein? Nein. Erlaubt ist für Variablen eine
Menge. Du kannst groß- und kleinschreiben, Zahlen reinnehmen,
Unterstriche dürfen auch drin sein. Python-Befehle dürfen nicht
verwendet werden, da diese reserviert sind (die lernst Du jetzt mit der
Zeit). Eine Variable darf jedoch nicht mit einer Zahl anfangen. Dann
gibt es ein paar Regeln, die zwar nicht zwingend sind, bei denen Du aber
mit Zeit immer mehr sehen wirst, dass es total Sinn macht, sich diese
von Anfang an anzugewöhnen: Variablennamen sollten immer klein anfangen,
außer es handelt sich um eine Klasse. Was das ist, kommt ein ganzes
Stück später. Merke Dir jetzt nur - sie beginnen mit einem kleinen
Buchstaben. Unterstriche sind zwar erlaubt, aber nicht üblich. Besser
ist die sogenannte CamelCaseSchreibweise, also verschiedene Wörter
zusammenzusetzen und mit einem großen Buchstaben anzufangen und zusammen
zu schreiben. Und Variablen sollten einen zeigen, was sie machen. Müsste
es dann nicht besser „Schrittzähler" statt „i" heißen? Jein. Bei
Schleifen gibt es quasi die Ausnahme von der Regel, dass i (und bei
mehreren ineinander greifenden Schleifen j, k etc.) üblich sind. Die
Variablen sollten gleichzeitig möglichst kurz sein, damit man nicht so
viel schreiben muss, aber lang genug, dass man sie versteht. Das kommt
also immer auf das konkrete Problem an. Wenn Du drei Koordinaten
definieren willst, können x, y und z super Variablennamen sein. Wenn Du
aber mehrere hast, hilft x1 und x2 vielleicht nicht mehr weiter, weil Du
jedes mal neu überlegen musst, wofür x1 und x2 stehen sollte. Das ganze
sind keine starren Regeln, Du wirst sehen, dass wir gerade bei kleinen
Codestücken uns ebenso nicht sklavisch daran halte. Behalte sie jedoch
im Hinterkopf und Du wirst mit der Zeit ein Gefühl dafür gewinnen, wie
gute Bezeichnungen aussehen. Das mag sich an dieser Stelle noch komisch
anhören, ist aber tatsächlich ein wichtiges Thema. Und als letzte Regel
- wähle die Namen möglichst in Englisch. Wenn Dein Englisch noch nicht
so gut ist, ist das am Anfang noch nicht so wichtig. Aber später wirst
Du in größeren Gruppen coden. Dank des Internets passiert das nicht
unbedingt nur mit Menschen, die Deutsch sprechen. Deshalb macht es Sinn,
gleich alles in Englisch zu machen.

Rechnen
-------

Computer heißen nicht umsonst auch Rechner. Die „üblichen"
Rechenoperationen funktionieren hier natürlich auch. Das wollen wir uns
mit ein wenig Code anschauen. Lösch Deinen bereits geschriebenen Code,
schreib den Mustercode ab und schau Dir die Ausgabe an.

a = 10 b = 5 print(\"10 + 5 =\", a + b)

Hier kurz die wichtigsten Rechenarten, die Du in Python nutzen kannst:

-   \+ addieren

-   -- substrahieren

-   \* multiplizieren

-   / dividieren

-   // Ganzzahldivision (teste 22/8 und 22//8)

-   \% Modulo (gibt den Rest aus, teste 22%8)

-   \*\* für Potenzen, also 3-Quadrat ist 3\*\*2

Diese Klammern ( ) kannst Du auch verwenden. Wie beim Taschenrechner
werden „Kommazahlen" mit einem Punkt ausgegeben. Spiele damit ein wenig
rum und probiere auch mal aus, ob die Regle Punkt-vor-Strich beachtet
wird und wie mit Klammern diese Regel „umgangen" werden kann.

Versuche bitte folgendes auszurechnen:

$$(\frac{1}{2} - \frac{1}{4} + \frac{4+3}{8} ) * 2$$

Wenn Du richtig gerechnet hast, sollte 2.25 rauskommen. Wenn nicht, hast
du vielleicht ein falsches Rechenzeichen gesetzt oder die Klammern nicht
richtig positioniert.

Es gibt noch mehr Möglichkeiten, wie beispielsweise Wurzelziehen. Dafür
muss aber ein zusätzliches Modul eingebunden werden. Dazu kommen wir
noch.

Eingaben
--------

Jetzt wollen wir den/die Benutzer:in einbinden. Die Eingabe einer
Benutzerin weisen wir einfach einer Variable zu. Und wir sagen dem
Benutzer noch, was er oder sie hier eingeben soll. Das schaut so aus:

userInput = input(\"Bitte gib eine Zahl ein: \")

Jetzt bist Du wieder dran. Kombiniere das gelernte. Frage die Benutzerin
nach zwei Zahlen, addiere sie und gib sie aus. Wenn das funktioniert,
lege um den Codde eine For-Schleife herum, die dafür sorgt, dass der
Code dreimal ausgeführt wird. Teste ihn. Kommen beim zweiten Anlauf
komische Ergebnisse heraus? Versuche rauszubekommen, woran das liegt. Es
hilft, sich bei jeder Zeile zu überlegen, was diese macht und welche
Variablen gerade welchen Wert haben. Falls Du hier zu keinen Ergebnis
mehr kommst, lass uns in einem virtuellen CoderDodo gemeinsam drauf
schauen. Probiere auch mal aus, was passiert, wenn man einen Buchstaben,
statt eine Zahl eingibt. Wie man solche Fehleingaben der Benutzenden
verhindert, kommt etwas später.

Bedingungen
-----------

Kommen wir zu einem neuen Element: Bedingungen. Es gibt recht häufig
Situationen, in denen etwas zu entscheiden ist. Dabei müssen etwas
vergleichen. Da ein Gleichheitszeichen bereits mit der Zuweisung für
eine Variable belegt ist, nimmt man einfach zwei Gleichheitszeichen. Das
folgende Beispiel ist sehr simpel, zeigt Dir aber, wie es funktioniert:

a = input(\"Bitte gib eine Zahl ein: \") b = input(\"Bitte gib noch eine
Zahl ein: \") if a == b: print(\"a und b sind gleich\") else: print(\"a
und b sind ungleich\")

Es kommt häufiger vor, dass wir mehrere Vergleiche machen müssen. Wie
das ausschuat, zeigt das nächste Beispiel. Die Zeichen „\<", „\>", „\<="
und „\>=" solltest Du bereits aus der Schule kennen. Um zusagen, dass
etwas ungleich ist, schreibt man „!=".

a = input(\"Bitte gib eine Zahl ein: \") b = input(\"Bitte gib noch eine
Zahl ein: \") if a == b: print(\"a und b sind gleich\") elif a \< b:
print(\"a ist kleiner als b\") elif a \> b: print(\"b ist kleiner als
a\") else: print(\"Ich bin verwirrt.\") print(\"Habe fertig.\")

Das Programm springt also in if und führt einen Vergleich durch. Ist der
erfolgreich, springt es gleich an das Ende dieser Bedigung. In die
anderen Codeteile schaut es gar nicht mehr herein. In den else-Teil
kommt es nur dann rein, wenn keiner der vorher aufgeführten Bedingungen
wahr war. Alles klar soweit? Dann hast Du schon wirklich wesentliche
Elemente gelernt, mit denen man schon eine Menge machen kannst.

Übungen
-------

Auf geht es zu ein paar Übungen, um das neu erlernte zu festigen.

### Grundumsatz

Mit der sogenannten Harris-Benedict-Formel lässt sich (ungefähr)
berechnen, wieviel Kilokalorien ein Mensch pro Tag verbraucht. Bei
Männern lautet diese:

G = 66,47 + 13,7 \* m + 5 \* l - 6,8 \* t

und bei Frauen:

G = 655,1 + 9,6 \* m + 1,8 \* l - 4,7 \* t

Denk bitte daran, dass der Dezimaltrenner bei python (und quasi allen
anderen Programmiersprachen) der Punkt und nicht das Komma ist.\
G ist dabei der Grundumsatz, m steht für das Körpergewicht in kg, l für
die Körpergröße in cm und t für das Alter in Jahren. Schreibe ein
Programm, dass die Nutzenden die notwendigen Informationen abfragt und
dann den Grundumsatz auswirft. Dieser wird in Kalorien, richtigerweise
in Kilokalorien ausgegeben. Da diese Einheit zwar gebräuchlich, aber
veraltet ist, gib das Ergebnis auch in Kilojoule aus. Eine Kalorie
entspricht ungefähr 4,184 Joule. Wenn Du damit ins Stocken kommst,
versuche erstmal Testwerte (bsp. 80 kg, 170 cm und 25 Jahre) den
Variablen zuzuweisen und die Ergebnisse für Männer und Frauen beide
ausgeben zu lassen.

### Summen

Bitte frage den Benutzer nach einer Zahl. Addiere dann alle Zahlen von 1
bis zur Zahl die die Benutzerin Dir angegeben hat und gib das Ergebbis
aus. Beispielsweise Du bekommst eine 5 übergeben, dann soll die Rechnung
1+2+3+4+5 lauten.

### Schach mit ASCII-Art

Gib ein Schachbrett (8x8-Feld) aus. Dabei sollen die weißen Felder mit
einer 0 dargestellt werden und die schwarzen Felder mit einer \#.
Verwende dafür For-Schleifen. Auch hier gilt - weniger Code ist mehr.
Wenn Du den Code fertig hast und die Ausgabe stimmt - nimm Dir Zeit und
überlege, ob es nicht doch noch eine Lösung gibt, mit der Du das
Ergebnis mit einer einfacheren Lösung hinbekommst. Und auch wenn Dir ein
erster Lösungsansatz nicht einfällt - Geduld. Grübeln und probieren
gehört dazu.

Variablentypen und Stringspielereien
------------------------------------

Inzwischen hast Du schon recht viel mit Variablen gearbeitet. Ist Dir
dabei aufgefallen, dass bei einem a = input(\"Gib was ein: \") und a = 1
das erste a für einen Text steht und das zweite für eine Zahl? In vielen
Programmiersprachen muss man bevor man eine Variable das erste mal
nutzen kann, dem Rechner sagen, wie Du diese Variable verwenden willst.
Das brauchst Du in Python nicht. Das hat so seine Vor- und Nachteile.
Steht eine Variable für einen Text, verwendet man sie als String,
abgekürzt str. Bei natürlichen Zahlen nennt man sie Integer, abgekürzt
int. Mit type(a) kannst Du Dir ausgeben lassen, wie die Variable
verwendet wird. Probiere mal folgenden Code aus:

a = \"Das ist Text\" print(type(a)) a = 1 print(type(a))

Ja, und? Macht das einen Unterschied? Probiere mal folgenden Code aus:

number1 = 5 number2 = 3 text = \"Quatsch\" print(type(number1))
print(type(number2)) print(type(text)) print(number1 + number2)
print(number1 + text)

Du kannst python auch anweisen, dass Variablen einen bestimmten Typ
annehmen. Mit int(a) sorgst Du dafür, dass a als Zahl behandelt wird und
mit str(a), dass es als Text verwendet wird. Probiere folgendes aus:

number1 = 5 number2 = 3 print(type(number1) print(number1 + number2)
number1 = str(number1) number2 = str(number2) print(number1 + number2)

Und jetzt probieren wird es in die andere Richtung:

text1 = \"alles\" text2 = \" Quatsch\" print (text1 + text2)

Jetzt für vor dem print noch ein text1 = int(text1) ein und führe den
Code aus. Da wird es „knallen", will sagen, es gibt eine Fehlermeldung.
Warum ist das so? Weil Du Zahlen nicht als Integer behandeln kannst. Mit
einem

if type(a) == int:

kannst Du feststellen, ob a eine Zahl ist. Oben hattest Du einen
einfachen Taschenrechner gebaut. Der hat zwei Zahlen addiert, die die
Benutzerin eingegeben hat. Alles ist gut, bis der Mensch ins Spiel kommt
- denn wenn der Benutzer eine einen Buchstaben eingibt, kann er nicht
mehr addieren, sondern setzt bestenfalls die beiden Abfragen zusammen.
Du hast jetzt gelernt warum das so ist. Jetzt schreibe das Programm so,
dass es prüft, ob der Benutzer einen Integer eingegeben hat. Falls
nicht, soll die Abfrage nochmal durchlaufen werden und der Hinweis
erfolgen, dass nur ganze Zahlen erlaubt sind.

Wir wollen uns ein paar weitere Variablentypen anschauen:

a = 3.14 print(type(a)) a = True print(type(a)) a = False print(type(a))

Der erste Typ nennt sich Float und ist für sogenannte Gleitkommazahlen.
Spätestens wenn Du dividierst oder mit Prozenten arbeitest, tauchen sie
auf. Hier gibt es aber echte Untiefen, da Computer nicht so rechnen, wie
wir das erwarten. Auf das Thema werden wir noch mehrfach zurückkommen.

Interessant ist der zweite Typ. Der nennt sich Bool oder Boolean
(gesprochen Buhl bzw. Buhleo). Das ist ein Gedenken an den
amerikanischen Mathematiker George Boole, der im 19. Jahrhundert lebte.
Dieser Variablentyp kann nur zwischen Wahr und Falsch unterscheiden. Das
klingt banal, aber Du wirst sehen, dass er sehr praktisch ist und häufig
Verwendung findet.

Nein, das war noch nicht alles. Python kennt noch mehr Variablentypen.
Aber an dieser Stelle sollen uns die erst einmal genügen. Python
versucht den Typ der Variable passend für Dich umzuwandenln. Wenn Du
also einen String mit einem Integer addierst, würde in vielen Sprachen
ein Fehler ausgeworfen werden. Python wandelt automatisch den Integer in
einen String um - und wenn zwei Strings „addiert" werden, dann hängt
Python sie einfach hintereinander. Die andere Richtung geht nicht
unbedingt. Denn ein String kann nur in einen Integer umgewandelt werden,
wenn er nur aus Zahlen besteht. Fluch und Segen hängen oft nahe bei
einander: Auf der einen Seite kann Dir diese automatische Umwandlung
manch einen Programmabsturz ersparen. Auf der anderen Seite hast Du
Deinen Code nicht richtig im Griff, sonst hättest Du wohl eher
ausdrücklich (wir sagen explizit) die Variable umgewandelt. Will sagen -
wenn a = 1 und b = "a" ist, dann ist a + b = 1a. Denn a wird zu einem
String umgewandelt und beide werden kombiniert. Würde aber b einmal eine
Zahl sein, dann würde Python nicht umwandeln, sondern sie addieren. Puh.
Du müsstest also sicher sagen können, was in den Variablen drin steckt.
Sicherer ist es in diesem Fall zu schreiben str(a) + str(b). Dann kannst
Du Dir sicher sein, was jetzt passiert. Dieses Thema ist anfangs etwas
abstrakt, aber wir kommen da so früh drauf, weil hier echte Untiefen
lauern. Wenn Du das Gefühl hast, Du bekommst „komische" Ergebnisse oder
der Code läuft einfach nicht, schau Dir immer auch an, was die einzelnen
Variablen gerade so machen. Wenn Du Dir unsicher bist, definiere sie
explizit, also mit Variablentyp. Keine Sorge, wir werden das Thema noch
üben und immer wieder darauf zurückkommen. Wichtig ist an dieser Stelle,
dass Du schon davon gehört hast und versucht, einen Blick dafür zu
entwickeln, mit was für einem Variablentyp Du es gerade zu tun hast. Im
Code kannst Du immer mit type(Variablenname) Dir den auswerfen lassen.

l6cm ![image](Pictures/SolarpanelBp.jpeg){width="5.5cm"}

Die Überschrift hieß nicht nur Variablentypen, sondern auch
Stringspielereien. Wie man zwei Strings wie "ot" "to" kombiniert, hast
Du schon gesehen: aus "ot" + "to" wird "otto". Das ein String in zwei "
" eingefasst werden muss, haben wir noch nicht explizit gesagt, ist Dir
aber sicherlich schon aufgefallen. Du kannst auch das \' nehnen, also
das Zeichen über der Raute. Beides geht, Du solltest es nur nicht in
einem Code mischen. Aber was machst Du nun, wenn Du in einem String
sagen wolltest: print(Und er sagt: \"Hallo\" ). Nur zu, hack das ein.
Soviel vorweg - das wird nichts. Denn Python liest das erste und weiß -
jetzt geht der String los. Dann kommt das zweite und er denkt, der
String ist zu Ende. Und dann kommt etwas, mit er echt nichts anfangen
kann - „Hallo". Typisch menschlicher Kram halt. Hier gibt es zwei Wege.
Entweder Du schreibst print(Und er sagt: 'Hallo' ) bzw. print(' Und er
sagt: "Hallo" '). Oder Du sagst ihm, dass das folgende Zeichen nicht in
weiter beachtet werden soll, dafür benutzen wir den sogenannten
Backslash. Das ist der Querstrich beim ß, den Du mit AltGr ansteuerst.
Du würdest also schreiben print(Und er sagt: \\Hallo \\ ). Python liest
die ersten Anführungszeichen und weiß, der String geht los. Bei den
zweiten kommt erst der \\-- da sagt sich Python -- nicht nachdenken,
stumpf ausgeben. Wir nennen diesen Vorgang escapen, von entkommen oder
aussteigen. Und nur damit Du es gesehen hast - man kann auch mit drei "'
arbeiten, um einen String zu definieren. Das funktioniert sogar über
Zeilen hinweg.

Die Ausgabe von Variablen hast Du schon gesehen, auch die Kombination
mit Text. Dafür gibt es einen einfachen und einen eleganten Weg. Der
einfache Weg ist print(Inhalt von a: , a). Der ist aber veraltet und
wenig elegant. Besser ist:

inhalt = 1000 print(f\"Der Inhalt ist: inhalt\")

Probier mal aus! Falls Dir mal solche Konstrukte mit einem %-Zeichen
über den Weg laufen - das ist eine veraltete Methode, um Strings in
Python zu formatieren. Ein Vorteil der neuen Methode ist, dass das f
gerüchteweise nicht nur für format sondern auch für fast steht. Der Code
kann damit schneller ausgeführt werden. Das funktioniert auch mit
mehreren Variablen:

vorname = \"Guido\" nachname = \"van Rossum\" print(f\"Die
Programmiersprache Python wurde von vorname nachname erfunden.\")

Auch solche „Spielereien" funktionieren:

inhalt = 1000 print(f\"Der Inhalt hat sich vermehrt - inhalt + 100\")

Wir werden später noch einmal auf das Thema zurückkommen, weil sich so
beispielsweise auch etwas wie ein Datum passend formatieren lassen.

Browser, E-Mail und Messenger
-----------------------------

Welchen Browser benutzt Du eigentlich? Safari, Chrome, Firefox oder
Edge? Wir werden uns später noch genauer ansehen, was passiert, wenn Du
mit einem Browser auf eine Internetadresse zugreifst. An dieser Stelle
aber schon einmal soviel - Du überträgst immer eine Menge Daten, die
viel über Dich verraten können. Klar könnte man jetzt sage, egal, was
soll schon jemand damit anfangen. Aber vielleicht suchst Du auch mal
nach Dingen, die andere nichts angehen, besuchst Seiten, die Unbekannte
nichts angehen und vor allem nicht, dass sie Dir später vorgehalten
werden können. Dir muss immer klar sein - Daten die von Unternehmen
gesammelt werden können, bleiben im Zweifel dort und gehen dann wann und
auch einmal verloren. Vielleicht interessiert Dich jetzt ein Mensch und
später möchtest Du damit nicht später aufgezogen werden. Um das
bildlicher zu machen - stell Dir vor, Dein Eltern schauten die ganze
Zeit Dir über die Schultern ...

l8.5cm ![image](Pictures/children-593313_1280.jpg){width="8cm"}

Es gibt Browser, die besonders gerne Daten sammeln und manche auch nach
Hause schicken ebenso wie es Browser gibt, die anderen Seiten es
leichter oder schwerer machen, dies zu erkennen, wer Du bist. Aktuell
empfehlen wir Dir Brave zu benutzen, da er recht datensparsam ist.
Abraten würden wir Dir klar von Edge und Chrome. Brave baut auf Chrome
auf, ist aber eben datenschutzfreundlicher. Diese Empfehlung gilt jetzt
- Software ändert sich. Da gilt es dran zu bleiben.

r4cm ![image](Pictures/Brave.png){width="4.5cm"}

E-Mail ist ein ganz großes Thema. Deutlich später wollen wir ebenfalls
einmal dran zu machen zu schauen, wie das eigentlich so funktioniert.
Das ist aber ein eigenes und größeres Kapitel. Wir gehen davon aus, dass
Du keinen eigenen E-Mail-Server betreust, sondern noch keine
E-Mail-Adresse hast oder eine bei den bekannten Anbietern. Hier willigst
Du teilweise sogar ausdrücklich ein, dass sie Deine E-Mails mitlesen
dürfen, um Werbung für Dich zusammen zu stellen. Was soll da schon
schief gehen ...Vorab - bevor Du Dir irgendwo ein Konto anlegst, sprich
bitte mit Deinen Eltern darüber! E-Mails zu unterhalten kostet Geld.
Entweder das wird über Werbung finanziert (wie bei Google, gmx und
anderen) oder Du zahlst dafür. Dienste wie posteo bieten Dir preiswerte
E-Mails an, bei denen viel dafür spricht, dass sie nicht mitlesen (weil
sie ihr Geld eben anders verdienen). Vielleicht sind Deine Eltern
bereit, Dir das zu bezahlen. Oder sie schenken Dir eine
Fördermitgliedschaft bei cyber4EDU e. V. und mit dem Mitgliedsbeitrag
wird die Arbeit für freie, datenschutzkonforme Bildung gefördert. Da
bekommst Du auch eine E-Mail-Adresse. Der einzige kostenlose Anbieter,
den wir derzeit empfehlen können ist protonmail.com. Der Speicherplatz
ist begrenzt, sollte aber für die meisten Sachen ausreichen. Neben dem
Aspekt des Datenschutzes gibt es noch einen anderen, der nicht vergessen
werden will. Um so mehr Du programmierst, um so mehr wirst Du Dich mit
anderen austauschen - und da sehen andere immer wieder auch Deine
E-Mail-Adresse. Mit einer Adresse von hotmail oder ähnlichen Anbietern
läufst Du rum wie mit einer Clownsnase und rosa Plüschsocken. Das kannst
Du besser (Plüschsocken sind natürlich eigentlich okay).

Und als letztes zu den Messengern. Du hast bestimmt schon gehört, dass
es da gute und weniger gute gibt. Fangen wir gleich mit den
Schlusslichtern an -- WhatsApp. Schmeiß das am besten gleich von Deinem
Handy runter. Bei WhatsApp sendest Du jede Telefonnummer in Deinen
Kontakten zu WhatsApp und damit zu Facebook -- selbst wenn deren
Inhaberinnen kein WhatsApp haben. Und Du hast die betreffenden Menschen
bestimmt vorher nicht alle gefragt. Das ist nicht okay. Bring lieber
Deine Freundinnen dazu, vernünftige Messenger zu benutzen. Ja, Telegram
fällt da auch aus der Liste raus. Signal will leider zur Identifizierung
eine Telefonnummer, aber davon würden wir zumindest nicht abraten. Auch
Threema ist okay. Du wirst später lernen, dass dezentrale Strukturen
meist besser sind, als zentrale. Bei Signal braucht man unbedingt deren
Server. Das geht besser. Tauglich sind hierfür Konzepte wie von Jabber,
RocketChat und Matrix. Wir empfehlen an dieser Stelle ausdrücklich
Matrix - vor allem, weil Du uns da auch finden wirst. Da kann man sich
seinen eigenen Server schaffen. Und bei den Servern kann man dann
wählen, ob man unter sich bleiben möchte (also nur die Nutzer\*innen des
Servers können unter einander chatten) oder ob man darüber hinaus mit
anderen kommunizieren können will. Das nennt man bei Matrix föderiert
sein. Sprich mit Deinen Eltern -- und wenn sie nichts dagegen haben,
klick Dir bei beispielsweise bei matrix.cyber4edu.org einen kostenlosen
Account. Für Matrix gibt es verschiedenste Clients, die auf dem PC, im
Browser oder auf dem Handy laufen. Der bekannteste nennt sich Element.
Lade Dir den runter und richte ihn Dir ein. Uns findest Du unter ...

l7cm ![image](Pictures/Cyber4EDU_sticker.png){width="6.5cm"}

Okay, mit dem richtigen Browser, einer vernünftigen E-Mail-Adresse und
dem passenden Messenger ausgestattet bist Du von den Noobs erfolgreich
einen ganzen Schritt hin zu den Nerds gegangen. Du fängst langsam an,
Deinen Jogginganzug gegen einen Gi (das sind diese Anzüge für den
japanischen Kampfsport) einzutauschen. Das schaut gleich viel besser
aus!

Schleifen binden
----------------

Wir wollen zum Thema Schleifen zurückkehren und lernen, wie wir die
eleganter um Dinge herum binden können.

### Schleifen steuern

Zum Steuern von Schleifen lernen wir zwei neue Befehle kennen:

-   break

-   continue

Diese werden vor allem interessant, wenn Du in einer Schleife eine
Bedingung gesetzt hast, also eine if-else-Konstruktion. Bei *break* wird
die Schleife abgebrochen -- egal wieviel Schleifendurchläufe eigentlich
noch geplant gewesen wären.

Mit *continue* wird der Schleifendurchlauf ab dieser Stelle übersprungen
und es geht mit dem nächsten Durchlauf weiter. Wir zeigen Dir ein
Beispiel:

for i in range(5): print(\"Wir machen \") if i == 1: print(\"feinen
Fug\") elif i == 2: print(\"i: \", i) continue print(\"groben Unfug\")
elif i == 4: print(\" genug.\") break print(\"und so.\")

### While-Schleife

Neben der for-Schleife kennt Python noch einen Schleifentyp - die
while-Schleife. Diese lauft solange, bis die Bedingung im Kopfteil
erfüllt ist:

i = 0 while i != 10: question = input(\"Rate die Abbruchbedingung: \") i
= question

Okay, das Beispiel ist jetzt nicht der Bringer. Aber Du verstehst
hoffentlich, worum es geht. Eine Vorschleife läuft grundsätzlich ihr
Programm stupide ab, während die While-Schleife bei jedem Durchlauf die
Bedingung des Kopfteils prüft. Das kann man wunderbar für eine
endlos-Schleife verwenden:

while True: print(\"Das so lange weiter, wie die Bedingung wahr ist und
sie ist immer wahr.\")

Hier kannst Du entweder mit Strg+c bzw. Control+c den Ausstieg finden.
Das ist aber nicht gerade elegant. Mit break kannst Du sauber aus der
Schleife aussteigen. Wenn Du also solche endlos-Scheifen verwendest,
musst Du Dir immer überlegen, wie Du da wieder rauskommst. Wenn wir mit
einer grafischen Oberfläche arbeiten wird das unser Standardeinstieg
werden. Ansonsten kannst Du super für so kleine Tools benutzen. Hole Dir
nochmal das Programm oben für die Konfektionsgrößen hervor. Baue das
Programm so um, dass der Benutzer immer wieder die Möglichkeit bekommt,
die Größen neu ausrechnen zu lassen, bis er „quit" schreibt.

Listen
------

Als erstes eine Fingerübung: Wir wollen das „große" Einmaleins üben -
also die Multiplikationsreihen von 1\*1 bis 100\*100. Dafür sollen zwei
zufällige Zahlen ausgegeben werden und die Benutzerin gibt das Ergebnis
ein. Wir prüfen dann ob es richtig oder falsch ist und geben das
Ergebnis zurück. Für zufällig Zahlen gibt es so keinen Befehl in Python.
Wir müssten also erstmal etwas programmieren, dass zufällige Zahlen
auswirft. „Künstlich" zufällige Zahlen ausgeben zu lassen, ist gar nicht
einfach. Dankenswerterweise haben sich andere bereits die Arbeit gemacht
und wir können deren Code nutzen. Man spricht hier von Bibliotheken
beziehungsweise Libraries, die Du in Deinen Code einbindest. In Python
werden diese Bibliotheken auch Module genannt. Ein solches Modul stellt
Dir quasi neue, weitere Befehle zur Verfügung. Ein Prinzip von Python
ist es, dass Dir eigentlich alle notwendigen Bibliotheken mitgeliefert
werden (wie man noch weitere findet und einbindet -- Du weißt, später).
Das Modul für Zufallszahlen heißt „random". Es gibt zwei Möglichkeiten
es einzubinden -- entweder komplett oder nur den Befehl, den Du wirklich
brauchst. Das passiert ganz einfach:

import random

Hier laden wir nur den Befehl randrange aus dem Modul random:

from random import randrange

Warum muss das sein und es wird nicht einfach alles eingebunden, was
Python so mitliefert? Hinter jeder so einer Bibliothek stecken viele,
viele Zeilen Code. Um so mehr Du davon einbindest, um so langsamer wird
Dein Programm und um so mehr Speicher braucht es. Das merkst Du nicht,
wenn Du etwas wie random dazu lädst. Wenn Du aber 50 solcher
Bibliotheken laden würdest, wäre das schon weniger gut. Deswegen nutzen
wir import immer nur für die Teile, die wir wirklich brauchen. Und Du
ahnst es vielleicht schon - deswegen ist der Weg über „from Modul import
Funktion" der deutlich vorzugswürdigere.

Zurück zu unserer Aufgabe. Wie erstellen wir jetzt eine Zufallszahl? Wir
wollen, dass count1 eine Zufallszahl von 1 bis 100 zugewiesen bekommt.
Und damit wir sehen, dass das geht, lassen wir 20 solcher Zahlen
ausgeben.

from random import randrange for i in range(20): count1 = randrange(1,
100) print(count1)

Jetzt solltest Du alles haben für unseren Kopfrechentrainer: Weise
count1 eine Zufallszahl von 1 bis 100 zu, ebenso count2. Sag das dem
Benutzer an und frage die Benutzerin nach dem Ergebnis. Vergleiche es.
Wenn das stimmt, gibt es eine neue Aufgabe und wenn es falsch ist, sage
das und gib erneut die Möglichkeit, ein Ergebnis einzugeben. Diese
Gelegenheit erhalten Benutzende solange, bis das Ergebnis passt (wenn Du
willst, kann Du sie oder ihn nach drei Fehlversuchen auch „erlösen" und
die richtige Lösung verraten). Dann mal ran an die Tasten.

Das Programm läuft. Aber jetzt wollen wir es um einen Highscore
erweitern. Dafür muss am Anfang die Benutzerin nach dem Namen gefragt
werden. Das Spiel läuft so lange, bis als Antwort „fertig" eingegeben
wird. Für die Wertung wollen wir ein Verhältnis ausrechnen: Merke Dir,
wie viele Aufgaben der Benutzende gemacht hat und wie oft Fehler
auftraten. Wenn die Benutzerin „fertig" eingibt, wirfst Du dieses
Ergebnis als Wertung aus. Wenn Du das geschafft hast, kommen wir zum
nächsten Teil. Jetzt könnten wir uns diese Ergebnisse in einer Liste auf
einem Blatt aufschreiben - oder ...wir verwenden den Variablentyp Liste.
Der wird über zwei eckige Klammern definiert. Da können wir neue Daten
aufstapeln, sie abrufen und löschen. Entweder wird eine Liste gleich mit
Werten gefüllt. Dann sähe das so aus:

greatList = \[\"Antonia\", \"Johannes\", \"Lukasz\"\]

Mit print(greatList) können wir sie ausgeben. Die Datentypen innerhalb
einer Liste können alle möglichen sein, auch gemischt. Eine Liste kann
also auch so aussehen:

greaterList = \[\"Antonio\", 24, \"Johannes\"\]

Wenn wir ein bestimmtes Element ausgeben wollen, dass schreiben wir:

greaterList = \[\"Antonia\", \"Johannes\", \"Lukasz\"\]
print(greaterList\[1\])

Wait what? Warum steht da Johannes und nicht Antonia? Weil die Zählung
bei Listen immer bei Null beginnt. Und greaterList\[1\] ist damit das
zweite Element. In Programmiersprachen wir meist mit Null beim Zählen
angefangen. Es sollen schon Menschen gegeben haben, die aus Gewohnheit
mit dem Aufzug in den fünften Stock wollten, und dann die Taste „4"
gedrückt haben.

Wie fügen wir jetzt ein neues Element an:

greaterList = \[\"Antonia\", \"Johannes\", \"Lukasz\"\]
greaterList.append(\"Julia\") print(greaterList)

Für Listen gibt es noch eine ganze Reihe weitere Methoden. Hier soll uns
das erstmal genügen. Jetzt haben wir mit print(greaterList) diese Liste
ausgegeben. Aber was, wenn wir nicht die Liste auf einmal raushauen
wollen, sondern jedes Element einzeln. Stell Dir vor, in einer Liste
stecken Zahlen. Aber bei der Ausgabe soll zu jedem Listenelement noch
100 dazu addiert werden. Versuche mal selbst, wie Du es lösen würdest.
Wenn Du wissen willst, wieviele Elemente eine Liste enthält, dann geht
das mit anzahl = len(greaterList).

Wenn Du nicht geschmult hast, wird Dein Ergebnis vermutlich so oder
ähnlich ausschauen:

listOfNumbers = \[52, 235, 235, 93\] length = len(listOfNumbers) for i
in range(length): print(listOfNumbers\[i\])

Möglich wäre auch:

Jetzt wollten wir aber, dass die Zahl um 100 erhöht wird, also:

listOfNumbers = \[52, 235, 235, 93\] for i in listOfNumbers:
print(i+100)

Jetzt solltest Du es alleine hinbekommen, den Highscore noch dazu zu
entwickeln. Das Ergebnis -- also wieviele falsche im Verhältnis zu der
Anzahl der Versuchen war -- speicherst Du einfach mit zum Namen. Wandel
das Ergebnis also als String um und kombiniere es zum Namen. Einen
Hiweis noch: ein listenname.append(\"Element dazu hinzugefügt wird\")
setzt eine bereits bestehende Liste voraus. Deshalb musst Du die ganz am
Anfang erschaffen; wir sagen, initialisieren. Dafür schreibst Du einfach
highscore = \[\]. Versuche die Aufgabe stückchenweise abzuarbeiten. Wenn
Du nicht zu Rande kommst - gar nicht schlimm. Nicht verzweifeln, sondern
lass uns bei einer Videositzung gemeinsam drüber sprechen. Wenn Du da
Dein Problem nicht vor der Gruppe besprechen magst (was besser ist, weil
alle dann was lernen), dann gehen wir mit Dir gerne auch einen
Breakoutroom.

Funktionen
----------

So langsam wird Dein Code größer. Das verlangt nach besserer
Strukturierung. Und wirst an den Punkt kommen, an dem Du die selben
Codestücke mehrfach brauchst. Für beides helfen Funktionen. Eine
Funktion hat einen Namen unter dem sie aufgerufen wir und sie kann (muss
aber nicht!) Parameter mitbekommen und kann auch Ergebnisse zurückgeben.
Das schauen wir uns gleich mal im Code an:

def hallo(): print(\"Hallo\")

hallo()

Jetzt wollen bauen wir eine printStrich-Funktion, die immer über und
unter dem Text einen Strich setzt:

def printStrich(content): print(\"------------------\") print(content)
print(\"------------------\") printStrich(\"Hallo\")

Und jetzt wollen wir noch Werte zurückgeben.

def add(a, b): c = a + b return c print(add(a,b))

Bei solchen Funktionen kann sogar direkt im return gerechnet werden:

def add(a, b): return a+b

Und als letztes Beispiel:

def calc(a,b): c = a + b d = a - b return c, d print(calc(5,4))

### Übungen {#übungen-1 .unnumbered}

Als erstes nimmst Du Dir jetzt bitte den Konfektionsgrößenrechner und
packst die Berechnung in eine Funktion.

Und weil es so schön war, nimmst Du Dir das vorherige Spiel. Überlege
Dir bitte, wie Du mit Funktionen das ganze übersichtlicher gestalten
könntest. Dabei sollte zumindest das Thema Highscore in einer Funktion
ausgelagert werden.

Jetzt knallt 's
---------------

Du hast ja bereits gesehen, dass Dein Code abstürzen kann. Meist hat das
zwei Ursachen - entweder Du hast einen Fehler gemacht oder der Benutzer
hat Dinge eingegeben, die an dieser Stelle da nicht hingehören. Dazu
zählen beispielsweise Buchstaben, wenn man nur Zahlen erwarten dürfte.
Streng genommen ist das aber auch Dein Fehler, denn Du musst immer damit
rechnen, dass Benutzende aus Versehen oder aus Böswilligkeit Dinge
eingeben, die Mist sind. Solche Fehler gibt es eine - oh Wunder -
Fehlerbehandlung. In Python heißt das try-except, aus anderen
Programmiersprachen wird der Begriff catch (engl. fangen) über den Weg
laufen. Das Prinzip ist einfach:

Was passiert hier? Im try-/Versuchs-Teil Du wirst nach einer Zahl
gefragt. Dann versucht er die Variable, der die Benutzereingabe
zugewiesen wurde, das in einen Integer umzuwandeln. Liegt kein Integer
vor, würde der Code jetzt abstürzen. Dann springt er in den Except-Teil.
Wird kein Fehler ausgeworfen, dann überspringt er den except-Teil. Ganz
stumpf könnte man jetzt so ein try-except um den ganzen Code ziehen,
damit er nicht mehr abstürzt. Das ist keine gute Idee. Wir wollen
fehlerfreien Code bauen und die Ausnahmebehandlung soll uns dabei
helfen. Das heißt auch, dass wir den Fehler möglichst gut einkreisen
wollen. Also lieber mehrere try-except-Funktionen verwenden. Besser wäre
schon:

Wenn es zum Except-Teil kommt, weißt Du jetzt, in welcher Zeile das
Problem auftritt. Aber das was wir hier sehen, ist immer noch nicht so,
wie Du es verwenden solltest. Dieser kurze Code soll Dir das Problem
zeigen (nicht abtippen!):

while True: try: print(\"Ich bin unaufhaltbar\") except:
print(\"Fehler!\")

Hier werden unendliche viele Fehler geworfen. Das Problem ist, dass Du
mit Strg-C den Code nicht gleich abbrechen kannst. Nicht gut. Deshalb
gewöhne Dir bitte an:

while True: try: print(\"Ich bin nicht mehr unaufhaltsam\") except
Exception: print(\"Fehler!\")

Jetzt wäre es ja noch hübsch, wenn er Dir anzeigte, was das für ein
Fehler genau ist. Wir zeigen Dir hier an dieser Stelle nur die
quick-and-dirty-Variante, um es jetzt nicht zu überladen. Das ist kein
guter Stil, aber kann hilfreich sein:

Wir kommen im nächsten Kyo darauf zurück und werden das Thema vertiefen.
Jetzt bist Du dran: Bitte nimm Dir das Programm für Kopfrechenübungen
wieder vor, dass Du gerade geschrieben hast. Sichere mit try-except ab,
dass nur sinnvolle Eingaben den Benutzenden verarbeitet werden. Weiste
die Benutzerin darauf hin, wenn eine Eingabe keinen Sinn macht. Gibt
dabei dem Benutzer unendlich viele Gelegenheiten, eine sinnvolle Eingabe
(also einen Integer) zu machen. Dann mal ran an die Tasten.

Hilfen
------

Zunächst einmal kannst Du anderen helfen, wenn Du Code schreibst, ihn
lesbar zu machen. Dafür verwendet man Kommentar. Einen Kommentar wertet
Python nicht aus. Er ist „nur" für den Mensch hinter dem Code.

print(\"Quatsch\") \# diese Zeile druckt Quatsch \# das wertet Python
nicht aus \"\"\" mit drei Hochkommata kann man mehrere Teile
auskommentieren \"\"\"\"

Kommentieren ist gar nicht so einfach. Beschreibe keine
selbstverständlichen Teile, dass verwirrt bloß (also nicht wie im
Beispielcode). Dafür solltest Du immer ein paar Worte verlieren, wenn
sich Code nicht selbst erklärt. Was macht die Funktion, welche Parameter
erwartet sie, was gibt sie zurück?

Daneben kann man Kommentar super nutzen, um Teile des Codes kurz nicht
zu verwenden, ohne sie gleich löschen zu müssen. PyCharm macht es Dir da
einfach: Markiere den Code und gib Strg+\# ein und der Code wird
auskommentiert. Machst Du das mit auskommentierten Code, wird er wieder
aktiv.

r4.5cm ![image](Pictures/help-2444110_1280.png){width="5cm"}

Wo kannst Du Dir Hilfe außerhalb der CoderDojo-Termine holen? Zu aller
erst natürlich in unserem Matrix-Kanal. Ansonsten empfehlen wir Dir
diese Internet-Seiten:

-   

Falls Du ein Freund von Büchern bist, würden wir Dir am Anfang folgendes
empfehlen:

-   als Anfängerlektüre:

-   zum Nachschlagen:

-   zum Üben:

Als Internetseiten empfehlen wir:

-   python-kurs.eu

Foren und Maillinglisten können sehr lehrreich sein, jedenfalls wenn es
die richtigen sind. Also um sowas wie gute-frage.de machst Du bitte
einen weiten Bogen. Bitte halte Dich in solchen Foren und Maillinglisten
an die Netiquette; ein höflicher Umgangston, ein Bitte und Danke,
sollten da wie im „echten" Leben auch verwendet werden. Bevor Du dort
etwas postet: Bitte lies eine Zeit lang mit, um so ein Gefühl für die
Umgebung dort zu bekommen. Bevor Du eine Frage stellst - bitte immer
vorher mit einer Suchmaschine und insbesondere im betreffenden Forum
schauen, ob die Frage schon gestellt wurde. Es ist wirklich lästig, wenn
die gleiche Frage das zehnte mal gestellt wird, weil der- oder diejenige
einfach zu faul war, vorher zu suchen. Falls Dir Maillinglisten nichts
sagen: Du trägst Dich dort mit Deiner E-Mail-Adresse ein. Schreibst Du
eine E-Mail an die Maillingliste, kriegen sie alle, die sich dort
eingetragen haben und Du bekommst umgekehrt auch alle Antworten. Meist
haben die auch ein Archiv, in dem man die alten Posts nachlesen kann.
Folgende Foren und Maillinglisten würden wir an dieser Stelle empfehlen:

-   

Wenn Du Bücher, Maillinglisten oder anderes gefunden hast, bei dem Du
denkst, dass die den anderen helfen - behalte das bitte nicht für Dich,
sondern teile es mit uns. Und wir schauen, ob wir es ins Script
aufnehmen.

Kultur
------

In der Einführung haben wir das versucht schon anklingen zu lassen --
uns geht es um mehr, als nur darum zu lernen, ein paar Zeilen Code in
den Rechner zu hämmern. Uns geht es auch um ein Stück Kultur. Deshalb
wollen wir Dir auch ein paar Bücher, Filme, Podcasts, Veranstaltungen
und mehr ans Herz legen. Wer gut coden will, muss auch kreativ sein.
Dazu gehört es mal raus an die frische Luft zu kommen und Sport zu
machen. Das steigert die (auch geistige) Leistungsfähigkeit enorm und
sorgt für mehr Zufriedenheit. Und wer konzentriert vor dem Rechner
sitzt, muss gelegentlich mal frische Luft an die grauen Zellen lassen.
An dieser Stelle wollen wir Dir ein Buch empfehlen: „Per Anhalter durch
die Galaxis" bzw. im Original „The Hitchhiker's Guide to the Galaxy" von
Douglas Adams. Es handelt sich um eine Triologie in fünf Bänden. Wenn
Dein Englisch gut genug ist, lies es im Original. Sonst halt in Deutsch.
Zumindest der erste Band ist Pflichtlektüre. Viele Witze unter Nerds
lassen sich ohne diese Literatur nicht verstehen. Falls Du schon alle
fünf Bände kennen solltest und im Original gelesen hast, solltest Du Dir
in einer Bibliothek die BBC-Fernsehserie besorgen.

l5cm ![image](Pictures/Matrix-logo.png){width="5cm"}

Unsere Filmempfehlung zum 8. Kyo sind die drei Teile von Matrix. Auch
hier gilt - Filme sind wie Bücher eine gute Gelegenheit, sein Englisch
zu trainieren. Wenn Dir das bei Filmen noch schwer fällt, probiere es
mit englischer Tonspur und englischen Untertiteln. Oder schau sie erst
auf Deutsch und dann nochmal (mit englischen) Untertiteln auf Englisch.

Schildkröte
-----------

Wir wollen ein wenig grafische Spielereien spielen. Dafür müssen wir
eine Bibliothek importieren. Wie das geht, hast Du schon gesehen. Hier
werden wir tatsächlich das ganze Modul importieren:

import turtle

Führ den Code aus. Wenn es einen Fehler gibt, dann muss turtle
nachinstalliert werden. Linux- und Macnutzer machen sich einen Terminal
auf und geben „pip install turtle" ein. Windows-Nutzer: Windows-Taste +
R, cmd eingeben, okay und dann „pip install turtle". Wenn das nicht
jeweils nicht hilft - bitte auf den Boden und auf Hilfe warten ...Im
Ernst - egal welches Betriebssystem Du verwendest, wenn Du das nicht
hinbekommst, lass Dir bitte helfen.

Die Zeilen kurz erläutert. Zunächst wird ein Stift (Pen) initialisiert.
Als nächstes wird die Hintergrundfarbe auf schwarz festgelegt. Danach
siehst Du einen praktischen Anwendungsfall für eine Liste, in der vier
Farben abgelegt werden. Die nächsten zwei Zeilen sollten klar sein.

Test
----

Der Weißgurt ist zum Greifen nahe. Schaffst Du die folgenden Fragen
beziehungsweise Aufgaben?

-   Schreibe das Spiel „Galgenmännchen". Falls Du es nicht kennst, lies
    Dir bitte den entsprechenden Wikipedia-Artikel durch. Lege eine
    Liste mit mindestens 20 Wörtern an, aus der für jedes Spiel ein
    zufälliges Wort ausgewählt wird. Der Spieler sieht die Anzahl der
    Buchstaben und kann immer einen Buchstaben raten. Fehlerhafte
    geratene Buchstaben werden in einer Zeile aufgezeigt, richtig
    geratene in das Lösungswort eingesetzt. Kommt es zu einem Fehler,
    wird in ASCII-Art ein Galgenmännchen in 15 Stufen gezeichnet. Wenn
    es Dir lieber ist, kannst Du das Galgenmännchen auch mit turtle
    zeichnen.

-   Was ist der einzige Zweck der Erde?

-   Schreibe eine Funktion, mit der Du mit Hilfe der Leibniz-Formel Pi
    berechnest. Dabei kannst Du von folgender Berechnung ausgehen:
    $$\pi = \frac{4}{1} - \frac{4}{3} - \frac{4}{5} - \frac{4}{7} - \frac{4}{9}$$
    Du siehst, dass der Zähler bei 4 bleibt, während sich der Nenner um
    2 erhöht. Die Funktion sollte einen Wert annehmen, der die Anzahl
    der Teile der Formel angibt. Um so höher dieser ist, um so genauer
    wird Pi. Vergleiche das Ergebnis beispielsweise mit dem Ergebnis auf
    dem Wikipedia-Artikel „Kreiszahl".

-   Was ist ein Vogel?

-   Zeichne mit turtle ein Dreieck und lasse es von rechts nach links
    über den Bildschirm „schweben".

-   Was ist die halbe Wahrheit?

Falls Du an einer Aufgabe festhängst - hei, kein Problem. Lass uns
gemeinsam drauf schauen, woran es hakt. Du schaffst das! Und falls Du
bei den Nicht-Programmieraufgaben nicht weiterkommst, lehnst Du Dich
entspannt zurück und nimmst Dir unsere Empfehlungen unter Kultur vor.

Alles geschafft? Wenn ja, dann **Herzlichen Glückwunsch**. Du bist
bereit für einen neuen Level. Du kannst stolz auf Dich sein - das hier
wahr schon eine ganze Menge neuer Stoff.

6. Kyo - Gelbgurt
=================

Überblick
---------

Herzlich Willkommen im Gelbgurt-Programm. Zunächst werden wir das
Versionsverwaltungssystem Git kennen lernen. Danach steigen wir in die
Bibliothek PyGames ein. Wie ganz am Anfang versprochen, werden wir uns
daran machen, ein Weltraumspiel zu programmieren. Programmiertechnisch
steht dabei im Vordergrund, das bereits erlernte zu festigen und zu
vertiefen. Wirklich neu werden Arrays sein und der erste Kontakt mit
Klassen. Zuletzt wollen wir einen ersten Blick auf html und css werfen
und mit flask erste Webseiten an den Start bringen.

Git - Klappe die erste
----------------------

Git gehört zu den sogenannten Versionsverwaltungssystemen. Diese lösen
mehrere praktische Probleme. Stell Dir vor, Du entwickelst ein etwas
größeres Projekt. Dann kann es immer sein, dass Du in eine falsche
Richtung abbiegst und wieder Teile rückgängig machen willst. Oder Du
magst einen neuen Teil einfügen, der aber nur experimentell zur
Verfügung stehen soll. Das Programm soll also in den Versionen stabil
und testing genutzt werden. Hierbei helfen Dir
Versionsverwaltungssysteme. Ihren größten Vorteil spielen sie aber aus,
wenn Du mit mehreren an einem Projekt arbeitest.
Versionsverwaltungssysteme sind alt und es gibt viele verschiedene.
Linus Thorvalds, der Erfinder des Linux-Kernels war aber mit allen
unzufrieden. Deshalb hat eine neue Versionsverwaltung geschrieben - Git.
Dieses ist inzwischen das dominante System und wird von Plattformen wie
github und gitlab verwendet. Vielleicht hast Du von denen schon gehört.
Für Git braucht es einen zentralen Server. Solche Plattformen können da
sehr hilfreich sein, wenn man selbst keinen betreiben möchte. Um Github
machen wir einen Bogen, da es von Microsoft gekauft wurde. Gitlab ist
okay, aber auch ein Unternehmen und vieles von dem, was die haben,
brauchen wir nicht. Wir könnten auch einen eigenen Server betreiben,
haben uns aber entschieden, aktuell unsere Energie anderweitig zu
investieren. Wir werden als Git-Server Codeberg verwenden. Das ist ein
Verein, der nicht mit den Daten Geld verdient, sondern sich über Spenden
finanziert. Bitte sprich mit Deinen Eltern darüber, ob es okay ist, dort
einen Account anzulegen. Und klick Dir dann bitte bei codeberg.de einen
Account.

l6cm

Git ist sehr mächtig. Deshalb wollen wir uns dem ganzen langsam nähern
und im Gelbgurt zunächst nur die elementaren Befehle lernen. Im
Orange-Gurt lernst Du, was man wissen muss, um ein Projekt zu managen.

Wir beginnen mit einem neuen Projekt. Du loggst Dich bitte in Coderberg
ein. Klicke dort auf das Pluszeichen neben Repository. Repository ist
quasi ein Softwareprojekt. Gib dem ganzen einen Namen. Wir beginnen hier
mit einem kleinen „helloWorld", um gleich zu üben. Den Rest der
Einstellungen lässt Du jetzt erstmal so stehen und klickst unten auf
„Repository erstellen". Wunderbar. Dein erstes Repository ist erstellt.
Codeberg gibt Dir jetzt eine kleine Anweisung wie es weitergeht. In
PyCharm legst Du bitte auch ein neues Projekt helloWorld an. Folgende
Beschreibung gilt für Linux- und MacNutzende. Windows-Nutzer zeigen wir
im Anschluss eine grafische Variante. Das ginge auch bei Linux und Mac,
aber tut Euch selbst einen Gefallen und nutzt die Gelegenheit, die
direkten Befehle zu üben. Nicht immmer hat man PyCharm zur Verfügung und
machmal kann man mit den Befehlen auch mehr erreichen. Die grafische
Anwendung macht nichts anderes, als genau die Befehle auszuführen, die
ihr jetzt schreibt. Aber bei so einer Anwendung bist Du immer davon
abhängig, welche Optionen Dir die Anwendung zur Verfügung stellt. Dabei
gibt es regelmäßig mehr, die Du vielleicht dann und wann nutzen
möchtest. Weiter geht's. Öffne bitte einen Terminal. Gehe in Dein
Verzeichnis: Mit *cd Pycharm* gehst Du in das Pycharm-Verzeichnis, mit
*cd helloWorld* gehst Du in Dein neues Projekt. Mit *ls -lah* kannst Du
Dir den Inhalt des Verzeichnisses anzeigen lassen. Da ist noch nichts
drin. Jetzt „arbeitest" Du die Coderberg-Anleitung ab:

touch README.md git init git checkout -b main git add README.md git
commit -m \"first commit\" git remote add origin
git\@codeberg.org:DeinBenutzername/helloWorld.git git push -u origin
main

Das brauchst Du nur beim ersten mal, wenn Du ein neues Projekt startest.
Windows-Nutzer machen sich bitte einen Explorer auf und gehen in das
Verzeichnis *helloWorld* an. Mit der rechten Maustaste kannst Du eine
neue Datei anlegen. Als Dateinamen gibst Du README.md an. In Pycharm
gehst Du auf VCS und dort auf *Create Git Repository*. Wähle Dein neues
Verzeichnis und klicke auf Okay.

r6cm ![image](Pictures/Git-logo.png){width="5.5cm"}

Gleich welches Betriebssystem Du nutzt, musst Du auf *VCS* in PyCharm
gehen und „Activate VersionsControlSystem" gehen. Du bekommst als
nächstes ein Auswahlmenü angezeigt. Wähle dort bitte Git aus.

In PyCharm gehst Du mit der Maus rechts auf Deinen Projektbaum und
wählst legst eine neue Python-Datei an. Benenne sie helloWorld. Pycharm
wird Dich fragen, ob Du diese Datei git hinzufügen möchtest. Du kannst
hier add klicken. Wir zeigen Dir aber auch gleich, wie man das von Hand
erledigt. Aber ob Du das von PyCharm machen lässt oder selber -- beides
hat seine Berechtigung. Zunächst mal zu den obigen Befehlen. Was ist da
passiert? Mich *touch* legt man eine leere Datei an. Mit *git init* wird
die git-Umgebung für dieses Projekt vorbereitet. Jetzt ist Dein
Verzeichnis für die Verwendung von Git vorbereitet. Dabei wird ein
Ordner .git angelegt. Der Punkt stammt aus der Unix-Welt (was bei Mac
unter der Haube steckt) und gilt ebenso für Linux. Verzeichnisse, die
mit einem Punkt anfangen, sind sogenannte verstecke Verzeichnisse. In
diesem Verzeichnis werden alle Änderungen „mitgeschrieben", die Du in
Git einpflegst und es wird die Konfiguration für dieses Projekt
angelegt. Den Befehl *git checkout* stellen wir noch für einen Moment
zurück.

r10cm

Mit git add fügst Du Dateien zu einem sogenannten Commit hinzu. Das kann
eine Datei oder können mehrere Dateien sein, die Du geändert hast und in
Dein git-Repository, also Deine „Code-Sammlung" schieben möchtest. Hier
siehst Du schon - Du entwickelst lokal, verwaltet wird der Code aber
zentral. Das ist nicht bei allen Versionsverwaltungssystemen so. Nicht
immer möchtest Du alle (geänderten) Dateien anderen mitteilen, deshalb
kannst Du bei *git add* einzelne Dateien hinzufügen. Wenn Du alle
Dateien aus Deinem Verzeichnis mitnehmen willst, dann kannst Du es Dir
einfach machen und *git add \** schreiben. Der „\*" steht für „alle".

Windows-Nutzende und diejenigen, die es grafisch wollen, erledigen
diesen Prozess über PyCharm. Wenn Du eine Datei änderst, fragt er
„Project configuration files can be added to Git. View Files / Always
Add / Don't Ask Again." Klicke auf „Always Add" und er fügt automatisch
alle dazu. Mac- und Linux-Nutzende können wenn sie mögen, auch diesen
bequemeren Schritt gehen. Den Schritt mit „*git add filename* können wir
mehrmals machen. Git merkt sich die einzelnen Dateien. Mit *git commit*
gehen wir den nächsten Schritt und schnüren quasi das Päckchen zu (wie
gesagt, bei dem Päckchen kann es sich auch nur um eine Datei handeln).
Wir müssen dem commit noch einen Kommentar mitgeben. Den sehen spätere
andere auch zu der jeweiligen Änderung. Es ist total hilfreich, wenn man
hier was sinnvolles eingibt. Also ein Kommentar wie \"nächste Änderung\"
hilft Dir gar nichts. Besser ist so etwas wie \"Fehlerkorrektur bei
Funktion printLinie". Das hilft Dir später enorm, Dinge wiederzufinden.
Wenn wir nur *git commit* eingeben, fragt er nach einem Kommentar. Das
können wir einfacher haben mit *git commit -m \"neue Funktion
Weltrettung\"*. Wer das über PyCharm lösen will, geht auf Git und dann
auf Commit. Und jetzt schieben wir diesen Commit auf unseren Server.
Aber wie so oft - das ist nicht sklavisch. Bei einem HelloWorld-Programm
reicht auch ein Hinweis auf „first commit" oder so.

Mit *git remote* verbinden wir das geschaffene zentrale git-Repository
mit unserem lokalen. Den Befehl brauchen wir normalerweise auch nur,
wenn wir mit einem neuen Projekt starten.

Und zuletzt kommt *git push*. Damit schieben wir alles, was wir im
Commit zusammengefasst haben jetzt in unser zentrales Repository.

Was Du quasi dauernd brauchst ist folgender Dreierschritt:

git add git commit git push

pygames - Spiele spielen
------------------------

Hier beginnt pygames.
