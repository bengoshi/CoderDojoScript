\babel@toc {ngerman}{}
\contentsline {chapter}{\numberline {1}Weißgurt}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Hello World}{1}{section.1.1}%
\contentsline {section}{\numberline {1.2}Schleifen}{4}{section.1.2}%
\contentsline {section}{\numberline {1.3}Was passiert in der Schleife?}{5}{section.1.3}%
\contentsline {section}{\numberline {1.4}Benennung von Variablen}{6}{section.1.4}%
\contentsline {section}{\numberline {1.5}Rechnen}{7}{section.1.5}%
\contentsline {section}{\numberline {1.6}Eingaben}{8}{section.1.6}%
\contentsline {section}{\numberline {1.7}Bedingungen}{8}{section.1.7}%
\contentsline {section}{\numberline {1.8}Übungen}{9}{section.1.8}%
\contentsline {subsection}{\numberline {1.8.1}Grundumsatz}{9}{subsection.1.8.1}%
\contentsline {subsection}{\numberline {1.8.2}Summen}{9}{subsection.1.8.2}%
\contentsline {subsection}{\numberline {1.8.3}Schach mit ASCII-Art}{9}{subsection.1.8.3}%
\contentsline {section}{\numberline {1.9}Variablentypen und Stringspielereien}{10}{section.1.9}%
\contentsline {section}{\numberline {1.10}Browser, E-Mail und Messenger}{13}{section.1.10}%
\contentsline {section}{\numberline {1.11}Schleifen binden}{15}{section.1.11}%
\contentsline {subsection}{\numberline {1.11.1}Schleifen steuern}{15}{subsection.1.11.1}%
\contentsline {subsection}{\numberline {1.11.2}While-Schleife}{15}{subsection.1.11.2}%
\contentsline {section}{\numberline {1.12}Listen}{16}{section.1.12}%
\contentsline {section}{\numberline {1.13}Funktionen}{18}{section.1.13}%
\contentsline {section}{\numberline {1.14}Jetzt knallt 's}{19}{section.1.14}%
\contentsline {section}{\numberline {1.15}Hilfen}{21}{section.1.15}%
\contentsline {section}{\numberline {1.16}Kultur}{22}{section.1.16}%
\contentsline {section}{\numberline {1.17}Test}{23}{section.1.17}%
\contentsline {chapter}{\numberline {2}Gelbgurt}{24}{chapter.2}%
\contentsline {section}{\numberline {2.1}Überblick}{24}{section.2.1}%
\contentsline {section}{\numberline {2.2}Git - Klappe die erste}{24}{section.2.2}%
\contentsline {section}{\numberline {2.3}pygames - Spiele spielen}{27}{section.2.3}%
